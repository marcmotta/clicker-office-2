///@arg frame_object
var _frame_object = argument0;

#region Validate Objects to be destroyed
if (_frame_object == o_cookie_frame) {
	
	if(instance_find(o_shop_frame,0)) {
		instance_destroy(instance_find(o_shop_frame,0));
	}
	
} 

if (_frame_object == o_shop_frame) {
	
	if(instance_find(o_cookie_frame, 0)){
		instance_destroy(instance_find(o_cookie_frame, 0));
	}
	
}
#endregion

#region Draw - Erase from scene
var _frame_object_exist_scene = instance_find(_frame_object, 0);

if (!_frame_object_exist_scene) {
	instance_create_layer(room_width / 2 + 2, room_height / 2, "hover", _frame_object);
} else {
	instance_destroy(_frame_object);
}
#endregion

audio_play_sound(s_tab, 2, false); 

