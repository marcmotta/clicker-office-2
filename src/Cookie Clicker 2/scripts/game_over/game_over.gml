//Bw screen
shader_set(shader_bw);



#region Slowdown Things
global.alow_chef_to_spawn = false;

var _bg_id = layer_background_get_id("bg")
layer_background_speed(_bg_id, 3);

var _chef = instance_find(o_chef, 0);
if(_chef != noone){
	_chef.movement_speed /= 2; 
	_chef.footsteps_interval = 30;
}

#endregion

#region Close Open Tabs

if(instance_exists(o_shop_frame) == true){
	instance_destroy(instance_find(o_shop_frame,0));}
	
if(instance_exists(o_cookie_frame) == true){
	instance_destroy(instance_find(o_cookie_frame,0));}

#endregion

	


	
