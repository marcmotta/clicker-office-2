///@arg0 slot
if(room != r_menu) exit;

var _y_initial_1 = 86;
var _y_initial_2 = 97;
var _y_offset = 11;
var _o_select = instance_find(o_select,0);


if (point_in_rectangle(mouse_x, mouse_y, 129, _y_initial_1 + (_y_offset * argument0) , 183 , _y_initial_2 + (_y_offset * argument0)) == true){
	_o_select.sel = argument0;
	y = _o_select.initial_y + _o_select.sep * _o_select.sel;
	
	if (mouse_check_button_pressed(mb_left)){
		audio_play_sound(s_selected, 5, false);
		if( _o_select.sel = 0)
		{
			room_goto(r_gameloop);
		}

		if( _o_select.sel = 1)
		{
			room_goto(r_options);
		}

		if( _o_select.sel = 2)
		{
			room_goto(r_credits);
		}

		if( _o_select.sel = 3)
		{
			game_end();
		}
		

	}
}
