
{
    "name": "r_options",
    "id": "16916a7a-490b-477c-9a99-eef6adcdfccf",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "04b27ae0-75a3-4c92-b5f8-203dca0cf31c",
        "27481e00-6dc8-474d-88d4-844566c09eca",
        "eb73abff-f242-4e60-bf9f-18097cab88d5",
        "479fa2dd-c80f-4f5e-8870-0479ef22af7f",
        "1814f079-d723-4958-95fc-ae7d14b21967",
        "2256c45f-7093-409b-9b1d-331f9ef579c8",
        "25585c3b-c9ac-4a09-9bc4-120b95df8a53",
        "aa9bfd64-6ddf-4cf6-a0a6-0970a1527142"
    ],
    "IsDnD": false,
    "layers": [
        {
            "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
            "name": "main",
            "id": "2249a6fa-f67d-4d28-b0ce-3cfd2f2af954",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "instances": [
{"name": "inst_1CBA5D51","id": "04b27ae0-75a3-4c92-b5f8-203dca0cf31c","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1CBA5D51","objId": "8eb74ecf-9b93-4682-8781-5af00ebe4072","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 0,"y": 0},
{"name": "inst_5C287230","id": "27481e00-6dc8-474d-88d4-844566c09eca","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5C287230","objId": "3bdcd590-798d-4694-b996-084c08c6d4db","properties": [{"id": "771a5320-3632-4cad-82df-477f0f7f81ab","modelName": "GMOverriddenProperty","objectId": "3bdcd590-798d-4694-b996-084c08c6d4db","propertyId": "fe4b33ee-3ee4-4008-880b-07749b2f78fd","mvc": "1.0","value": "3"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 245,"y": 153},
{"name": "inst_30E34A31","id": "eb73abff-f242-4e60-bf9f-18097cab88d5","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_30E34A31","objId": "3bdcd590-798d-4694-b996-084c08c6d4db","properties": [{"id": "3f4a2066-4380-4632-97bf-7933d9d14c5a","modelName": "GMOverriddenProperty","objectId": "3bdcd590-798d-4694-b996-084c08c6d4db","propertyId": "fe4b33ee-3ee4-4008-880b-07749b2f78fd","mvc": "1.0","value": "5"},{"id": "25c151f7-b003-4c81-9b04-fce822928601","modelName": "GMOverriddenProperty","objectId": "3bdcd590-798d-4694-b996-084c08c6d4db","propertyId": "6bf8e576-36f6-4983-a6f3-e789a7c056e6","mvc": "1.0","value": "1"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 140,"y": 82},
{"name": "inst_33AF5E25","id": "479fa2dd-c80f-4f5e-8870-0479ef22af7f","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_33AF5E25","objId": "3bdcd590-798d-4694-b996-084c08c6d4db","properties": [{"id": "88326736-e93f-4cc2-9265-45d905c14c04","modelName": "GMOverriddenProperty","objectId": "3bdcd590-798d-4694-b996-084c08c6d4db","propertyId": "fe4b33ee-3ee4-4008-880b-07749b2f78fd","mvc": "1.0","value": "5"},{"id": "b53385fa-6315-4ee1-a005-8139b681333e","modelName": "GMOverriddenProperty","objectId": "3bdcd590-798d-4694-b996-084c08c6d4db","propertyId": "6bf8e576-36f6-4983-a6f3-e789a7c056e6","mvc": "1.0","value": "0"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 140,"y": 72},
{"name": "inst_3B06A51E","id": "1814f079-d723-4958-95fc-ae7d14b21967","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3B06A51E","objId": "3bdcd590-798d-4694-b996-084c08c6d4db","properties": [{"id": "53f5d72f-5aa3-4775-aa94-144cf0b89672","modelName": "GMOverriddenProperty","objectId": "3bdcd590-798d-4694-b996-084c08c6d4db","propertyId": "fe4b33ee-3ee4-4008-880b-07749b2f78fd","mvc": "1.0","value": "5"},{"id": "90aecb47-7071-402f-9885-12c0bf4a1170","modelName": "GMOverriddenProperty","objectId": "3bdcd590-798d-4694-b996-084c08c6d4db","propertyId": "6bf8e576-36f6-4983-a6f3-e789a7c056e6","mvc": "1.0","value": "2"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 140,"y": 92},
{"name": "inst_5E9384DE","id": "2256c45f-7093-409b-9b1d-331f9ef579c8","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_5E9384DE","objId": "3bdcd590-798d-4694-b996-084c08c6d4db","properties": [{"id": "f7f745b2-b216-453f-a04e-c7b01aca2393","modelName": "GMOverriddenProperty","objectId": "3bdcd590-798d-4694-b996-084c08c6d4db","propertyId": "fe4b33ee-3ee4-4008-880b-07749b2f78fd","mvc": "1.0","value": "4"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 152,"y": 131},
{"name": "inst_60C6FD81","id": "25585c3b-c9ac-4a09-9bc4-120b95df8a53","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_60C6FD81","objId": "3bdcd590-798d-4694-b996-084c08c6d4db","properties": [{"id": "a131100e-9433-49cd-9c0e-d9a2a97b665c","modelName": "GMOverriddenProperty","objectId": "3bdcd590-798d-4694-b996-084c08c6d4db","propertyId": "fe4b33ee-3ee4-4008-880b-07749b2f78fd","mvc": "1.0","value": "6"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 246,"y": 70},
{"name": "inst_79B0B2D4","id": "aa9bfd64-6ddf-4cf6-a0a6-0970a1527142","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_79B0B2D4","objId": "3bdcd590-798d-4694-b996-084c08c6d4db","properties": [{"id": "308cc400-8713-410a-af57-2265ae4a27ed","modelName": "GMOverriddenProperty","objectId": "3bdcd590-798d-4694-b996-084c08c6d4db","propertyId": "fe4b33ee-3ee4-4008-880b-07749b2f78fd","mvc": "1.0","value": "6"},{"id": "13e297f2-3429-4a91-97bf-8c445aaae616","modelName": "GMOverriddenProperty","objectId": "3bdcd590-798d-4694-b996-084c08c6d4db","propertyId": "6bf8e576-36f6-4983-a6f3-e789a7c056e6","mvc": "1.0","value": "1"}],"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 191,"y": 70}
            ],
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRInstanceLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "bg",
            "id": "372ff531-12a0-4902-83ac-87c42e1996ba",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 100,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "b4d5dedd-1a09-4804-8e99-249e42a06c9a",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "792ceb92-1c46-48a7-a0f7-af9914529fa7",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "748fc06f-c100-4b39-986c-c1c5a2ed8174",
        "Height": 180,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 320
    },
    "mvc": "1.0",
    "views": [
{"id": "98359d7b-9777-4cd5-8762-7c9268a83dea","hborder": 32,"hport": 720,"hspeed": -1,"hview": 180,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": true,"vspeed": -1,"wport": 1280,"wview": 320,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "e84f8048-09ee-4edf-97fd-91628b01881c","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "51c5f561-e88b-41b0-b596-57abd3907830","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "569a0060-32b2-457d-bfdf-c1f49506f808","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ba01c3a7-6c53-4290-8cf0-a7a9a5706e18","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "f31c72a3-3a9d-44c7-bc5b-c449eec34201","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "ac5b5a95-b899-442f-adb3-53e2a32d79ac","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "325b79de-50a5-4baa-a040-dea8ccdadc90","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "c6d22429-87a1-4bd2-a70c-897afc61a9ca",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": true,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}