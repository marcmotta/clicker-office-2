var _frame_length = 7;

draw_sprite(sprite_index, image_index, x, y);

var _initial_top_frame_x = x - sprite_get_xoffset(sprite_index) + _frame_length;
var _initial_top_frame_y = y - sprite_get_yoffset(sprite_index) + _frame_length;
	   
//First Line
instance_create_layer(_initial_top_frame_x, _initial_top_frame_y, "hover", o_pwu_click);
instance_create_layer(o_pwu_click.x + o_pwu_click.sprite_width, _initial_top_frame_y, "hover", o_pwu_granma);
instance_create_layer(o_pwu_granma.x + o_pwu_granma.sprite_width, _initial_top_frame_y, "hover", o_pwu_farm);
instance_create_layer(o_pwu_farm.x + o_pwu_farm.sprite_width, _initial_top_frame_y, "hover", o_pwu_mine);


var _initial_bottom_frame_x = x - sprite_get_xoffset(sprite_index) + _frame_length;
var _initial_bottom_frame_y = y;

//Second Line
instance_create_layer(_initial_bottom_frame_x, _initial_bottom_frame_y, "hover", o_pwu_factory);
instance_create_layer(o_pwu_factory.x + o_pwu_factory.sprite_width, _initial_bottom_frame_y, "hover", o_pwu_bank);
instance_create_layer(o_pwu_bank.x + o_pwu_bank.sprite_width, _initial_bottom_frame_y, "hover", o_pwu_museum);
instance_create_layer(o_pwu_museum.x + o_pwu_museum.sprite_width, _initial_bottom_frame_y, "hover", o_pwu_space);

