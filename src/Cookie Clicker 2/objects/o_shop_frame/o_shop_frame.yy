{
    "id": "db03a1df-53dd-4ac0-879d-a66a321dda9a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_shop_frame",
    "eventList": [
        {
            "id": "5a5ee84a-b75b-4202-a5e4-e91607cf3bc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "db03a1df-53dd-4ac0-879d-a66a321dda9a"
        },
        {
            "id": "7aa9cda9-530f-41f9-b46f-7729815bd442",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "db03a1df-53dd-4ac0-879d-a66a321dda9a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5fb18e50-5548-42d4-b935-193becf28192",
    "visible": true
}