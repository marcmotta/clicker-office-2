{
    "id": "2b589b97-2149-4bdd-9573-71c4b1001908",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cookie",
    "eventList": [
        {
            "id": "a13d27c6-7897-4c0e-b4b0-958571cded4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2b589b97-2149-4bdd-9573-71c4b1001908"
        },
        {
            "id": "289a5eaa-3eec-4e12-b72b-1226de6c639c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "2b589b97-2149-4bdd-9573-71c4b1001908"
        },
        {
            "id": "b630d12c-e818-4d85-a49b-f0a53ecbef17",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "2b589b97-2149-4bdd-9573-71c4b1001908"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "405555a0-bcfa-41b7-bb17-59b89a2a33d7",
    "visible": true
}