{
    "id": "8244263b-b8d7-4ce6-b1a8-67fbee4c98d8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_cookie_frame",
    "eventList": [
        {
            "id": "1ad5bc50-6595-449d-80be-c3cccc2188e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8244263b-b8d7-4ce6-b1a8-67fbee4c98d8"
        },
        {
            "id": "d40711d6-b837-47c4-ba8b-0a3f9c856d45",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "8244263b-b8d7-4ce6-b1a8-67fbee4c98d8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5e36c9e2-debd-4e63-ab6b-0330c598e88a",
    "visible": true
}