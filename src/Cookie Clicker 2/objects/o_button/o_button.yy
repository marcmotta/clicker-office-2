{
    "id": "3bdcd590-798d-4694-b996-084c08c6d4db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_button",
    "eventList": [
        {
            "id": "3182fc19-3b15-4f6d-9977-7773afb3a0d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3bdcd590-798d-4694-b996-084c08c6d4db"
        },
        {
            "id": "ca779624-c518-428f-83f0-3fcd3ae988f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3bdcd590-798d-4694-b996-084c08c6d4db"
        },
        {
            "id": "0106880c-bcd7-421d-8ae8-af420e221667",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "3bdcd590-798d-4694-b996-084c08c6d4db"
        },
        {
            "id": "d29d2c21-b084-4613-957a-1b9db3c95b3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3bdcd590-798d-4694-b996-084c08c6d4db"
        },
        {
            "id": "87a0dd09-1bb6-41a8-a24f-f2519d14c7ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "3bdcd590-798d-4694-b996-084c08c6d4db"
        },
        {
            "id": "21753d75-0c89-429f-b7c9-63f0063a474f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "3bdcd590-798d-4694-b996-084c08c6d4db"
        },
        {
            "id": "0882db56-ffa0-47e0-9ca4-8f34ff0f9ca4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3bdcd590-798d-4694-b996-084c08c6d4db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "5025a30c-4b9e-485d-814f-65f65bf89dbe",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "afb4e309-d437-4f2c-9f36-0b9f8bc1fd8a",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "0067974f-ea97-459c-ac5e-620636dfa49d",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "b36e889b-d915-499c-8d39-b0deb28595bb",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "fe4b33ee-3ee4-4008-880b-07749b2f78fd",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "index",
            "varType": 0
        },
        {
            "id": "6bf8e576-36f6-4983-a6f3-e789a7c056e6",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "_var",
            "varType": 0
        },
        {
            "id": "30d824fc-48f7-459b-b544-5e8a652d8d7c",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "pressed ",
            "varType": 3
        }
    ],
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}