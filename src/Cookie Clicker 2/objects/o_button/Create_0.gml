
// 00 -- Start Button
button[0,0] = s_menu_start;
button[0,1] = 0 //Image Index;
button[0,2] = menu_start_game;

// 01 -- Options Button
button[1,0] = s_menu_options;
button[1,1] = 0 //Image Index;
button[1,2] = menu_options_tab;

// 02 -- Credits Button
button[2,0] = s_menu_credits; 
button[2,1] = 0 //Image Index;
button[2,2] = menu_credits_tab;

// 03 -- Exit Button
if room == r_options
	button[3,0] = s_options_hud_return;
else
	button[3,0] = s_menu_exit;
button[3,1] = 0 //Image Index;
button[3,2] = menu_game_exit;

// 04 -- Checker Button
button[4,0] = s_options_hud_checker; 
button[4,1] = 0 //Image Index;
button[4,2] = menu_checker_sandbox;
checked = false;

// 05 -- Slider Button
button[5,0] = s_options_hud_slider; 
button[5,1] = 0 //Image Index;
button[5,2] = false;

if(index == 5){
	_maximum = x;
	_minimum = x - 50;
	
	_volume_current = (140 + 50 - x)*0.02;
}

// 06 -- Toggle Button
button[6,0] = s_options_hud_toggle; 
button[6,1] = 0 //Image Index;
button[6,2] = false;



mask_index = button[index,0];

















