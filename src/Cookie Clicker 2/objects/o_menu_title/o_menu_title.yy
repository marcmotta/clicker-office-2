{
    "id": "c65a7407-c123-4cb4-842d-70a9b1b0e608",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_menu_title",
    "eventList": [
        {
            "id": "e35fd333-bedd-437f-a2fc-c7bac0646259",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c65a7407-c123-4cb4-842d-70a9b1b0e608"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "f192cee8-d0b8-4925-805d-36c511069079",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "sin_length",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "e7e782bb-1074-488c-82f1-fe406e7f8c98",
    "visible": true
}