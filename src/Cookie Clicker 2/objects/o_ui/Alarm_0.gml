/// @description spawn o_danger

/* chef direction
0 = left
1 = right
*/ 

chef_direction = choose(1,-1)

if chef_direction == 1 
instance_create_layer(1,52, "main", o_danger);
else 
instance_create_layer(room_width + 1, 52, "main", o_danger);