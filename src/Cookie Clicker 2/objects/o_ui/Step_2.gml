//danger spawn alarm set
if(room = r_gameloop){
	if(global.alow_chef_to_spawn == true){
		alarm[0] = alarm_range;
		global.alow_chef_to_spawn = false;
	}
}

#region Death Sceen Nerfs 

if(global.hp == 0){

	shader_set(shader_bw);

	//Sprites Slowdown

	var _bg_id = layer_background_get_id("bg")
	layer_background_speed(_bg_id, 3);
	
	global.alow_chef_to_spawn = false;
	var _chef = instance_find(o_chef, 0);
	if(instance_exists(o_chef)){
		_chef.movement_speed = 0.5; 
		_chef.footsteps_interval = 30;
		_chef.speed = 0.75 * chef_direction;
	}


	//Close Open Tabs
	if(instance_exists(o_shop_frame) == true){
		instance_destroy(instance_find(o_shop_frame,0));}
	
	if(instance_exists(o_cookie_frame) == true){
		instance_destroy(instance_find(o_cookie_frame,0));}
		
	
	
	//Create exit Button
	if create_exit_button == true
	{
		var _button_id = instance_create_layer(room_width/2, room_height/2 + 30,"hover",o_button);
		_button_id.index = 3;
		create_exit_button = false;
	}
		
}

#endregion

#region AUDIO

if(! audio_is_playing(s_music)){
	audio_play_sound(s_music, 3, true);
}

#endregion