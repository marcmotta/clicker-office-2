#region Window SETUP
display_set_gui_size(room_width, room_height);
window_set_cursor(cr_none);
global.lang = 0;
shader_reset();
audio_group_load(sg_effects);
audio_group_load(sg_oost);


#endregion

//Gameloop Global Variables
global.alow_chef_to_spawn = true;
global.persec = 1;
global.perclick = 1;
global.hp = 1;
score =	0;
alarm[1] = room_speed;





//Sandbox
if (variable_global_exists("global.sandbox")){ 
	if(global.sandbox == true and room == r_gameloop){
		var _bg_id = layer_background_get_id("bg");
		layer_background_change(_bg_id,env_bg_sb_strip6);
	}
}




//Create Font
global.f_pwup = font_add_sprite_ext(font_pwu, ".1234567890", false, 0);
global.f_cookie = font_add_sprite_ext(font_cookies_strip10, "c1234567890", false, 0);
global.f_psec = font_add_sprite_ext(font_cookiep_sec, "p/sec:.1234567890", false, 0);




#region Menu buttons

if(room == r_menu){
	var _separation = 15;
	var i = 0;

	repeat(4){
	
		var _button_id = instance_create_layer(room_width/2, room_height/2 + (_separation*i),"main",o_button);
		_button_id.index += i;
	
		i += 1;
	}
}

#endregion


#endregion 





