if (room == r_gameloop){
	draw_sprite(hud_plate, 0, room_width / 2, room_height);
	#region Score Counter
	draw_set_font(global.f_cookie);

		draw_set_halign(fa_middle);
		draw_set_valign(fa_bottom);
		
	draw_text(room_width / 2, room_height -7, "c " + string(score));
	
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);

	#endregion 	
	#region P/sec Counter
	draw_set_font(global.f_psec);
	
		draw_set_halign(fa_middle);
		draw_set_valign(fa_bottom);
		
	draw_text(room_width / 2, room_height, "p/sec:" + string(global.persec));
	
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);
		
	#endregion 
	#region HP Counter
	
	draw_sprite(hud_lifes_strip4, global.hp, room_width -2 , 2)

	#endregion
	#region Death Screen
	
	if(global.hp == 0){
		draw_sprite(hud_gameover, 0, room_width / 2, room_height/2);
	
		if(died == false){
			audio_play_sound(s_deathscreen, 5, false);
			died = true;
		}
		
	}
		
	#endregion
}




if (room == r_credits){
	
	#region Fade in and Fade out
	
	/* fade
	false = in
	true = out
	*/
	
	room_life_time += 1/room_speed;
	
	draw_sprite_ext(flash,1,0,0,1,1,0,0,image_alpha)
	
	if(fade = false){
		if (image_alpha > 0)
		image_alpha -= 0.005
	}
	if(fade = true){
		if (image_alpha < 1)
		image_alpha += 0.005
	else 
		room_goto(r_menu);
	}
	
	if (room_life_time > 54)
		fade = true;
	
	#endregion
	#region Credits_exit_button
	
	if(room_life_time > 5){
		
	if create_exit_button == true
	{
		var _button_id = instance_create_layer(room_width - 25, room_height - 15,"main",o_button);
		_button_id.index = 3;
		create_exit_button = false;
	}
	}
	
	#endregion
	#region Credits Text going up
	
	credits_text_y -= credits_text_speed;
	
	draw_sprite(credits_text_strip2, global.lang, room_width/2, room_height + credits_text_y);
	
	#endregion
	
}

#region Cursor Replace
    draw_sprite(hud_cursor, 0, mouse_x, mouse_y);
#endregion

