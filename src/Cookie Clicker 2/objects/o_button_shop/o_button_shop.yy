{
    "id": "daf4113c-57cb-4986-82a3-d7713736c6ca",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_button_shop",
    "eventList": [
        {
            "id": "5a0ef820-5688-4187-a05c-bab927c7c2f6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "daf4113c-57cb-4986-82a3-d7713736c6ca"
        },
        {
            "id": "1f63e854-2c85-4c66-8e57-c11f1063e222",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "daf4113c-57cb-4986-82a3-d7713736c6ca"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dafdd77f-2090-4e40-b329-5bd0a7691b71",
    "visible": true
}