{
    "id": "313fa52a-9e8f-4fb7-9117-13b81c4dcff4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_pwu_factory",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "de9c938e-999a-4e72-94b1-8aba9824388f",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "80ab55e6-e549-4bcc-a759-4f094648fbf2",
    "visible": true
}