{
    "id": "c493e6ca-57bf-4b79-8182-1bf4f2e7b666",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_button_cookie",
    "eventList": [
        {
            "id": "86759ab4-4ad5-4643-a478-37900f3084d4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c493e6ca-57bf-4b79-8182-1bf4f2e7b666"
        },
        {
            "id": "d1861ddf-8274-4c84-8e2c-0fd8c2b2d5a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "c493e6ca-57bf-4b79-8182-1bf4f2e7b666"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a74e7532-5e0b-4593-9632-25ba10d82dd3",
    "visible": true
}