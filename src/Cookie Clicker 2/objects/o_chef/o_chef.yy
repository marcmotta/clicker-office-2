{
    "id": "c83be337-352f-47ba-9235-1411b45d570e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_chef",
    "eventList": [
        {
            "id": "76687eb3-676d-4a7a-b61d-6a5ba90cf4cb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c83be337-352f-47ba-9235-1411b45d570e"
        },
        {
            "id": "54babda2-1be8-458f-ac07-ce688a915a08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "c83be337-352f-47ba-9235-1411b45d570e"
        },
        {
            "id": "7569dc02-9cce-4955-b68b-cd64d13a7d41",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8244263b-b8d7-4ce6-b1a8-67fbee4c98d8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c83be337-352f-47ba-9235-1411b45d570e"
        },
        {
            "id": "a5114e1a-a72b-4726-937f-cb85489bef0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "db03a1df-53dd-4ac0-879d-a66a321dda9a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c83be337-352f-47ba-9235-1411b45d570e"
        },
        {
            "id": "cb067ed3-5406-4bd5-be46-57eee0141ac3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c83be337-352f-47ba-9235-1411b45d570e"
        },
        {
            "id": "c93879ef-2a76-497d-a7fc-0c14b1a82ed8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c83be337-352f-47ba-9235-1411b45d570e"
        },
        {
            "id": "1cfa281e-dcb2-48a1-8a44-765293bcb2e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c83be337-352f-47ba-9235-1411b45d570e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "cfe98143-a743-4e8b-9739-680ebfb92bb4",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "1.5",
            "varName": "movement_speed",
            "varType": 0
        },
        {
            "id": "8843d900-bb93-4e7a-92f3-91b8f6c6d953",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "delay",
            "varType": 1
        },
        {
            "id": "971cac7a-2ab5-4f57-97e4-734f0aa40071",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "True",
            "varName": "alow_to_deal_damage",
            "varType": 3
        },
        {
            "id": "49c9a515-204b-44fc-9add-f78e017005e2",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "23",
            "varName": "footsteps_interval",
            "varType": 0
        },
        {
            "id": "0e66c443-bb27-4446-9a03-9d4e445eebc5",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "random_range(room_speed * 1, room_speed * 5);",
            "varName": "grr_interval",
            "varType": 4
        }
    ],
    "solid": false,
    "spriteId": "76878f65-43e1-4d41-8b88-701af96f3046",
    "visible": true
}