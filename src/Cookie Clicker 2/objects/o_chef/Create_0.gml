//footsteps alarm
alarm[0] = footsteps_interval;

//grr alarm
alarm[1] = grr_interval;

//initial grr
audio_play_sound(choose(s_chef_1, s_chef_2), 4, false);

//set mirror
var _ui = instance_find(o_ui, 0);
chef_direction = _ui.chef_direction;

if (chef_direction == -1){
	speed = movement_speed * -1;
	image_xscale = -1;
}
else
	speed = movement_speed;