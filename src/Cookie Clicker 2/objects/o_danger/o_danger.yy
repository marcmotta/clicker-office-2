{
    "id": "8695ff8c-0a6f-4c3b-b97e-696d2991f17b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_danger",
    "eventList": [
        {
            "id": "a802f020-6d86-4a7d-85a3-86e466bbed91",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8695ff8c-0a6f-4c3b-b97e-696d2991f17b"
        },
        {
            "id": "827a389e-f399-4e1f-941e-dc6415e4f74a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8695ff8c-0a6f-4c3b-b97e-696d2991f17b"
        },
        {
            "id": "8aa73361-87fd-468a-98b6-ef183c080934",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "8695ff8c-0a6f-4c3b-b97e-696d2991f17b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "9fabc711-5dfd-43a1-a46a-519de98ad151",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "2",
            "varName": "length",
            "varType": 1
        },
        {
            "id": "323d88a0-a9f7-42da-afb4-9e7e86a23f7a",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "40",
            "varName": "sfx_interval",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "b4edfbcb-ef77-4b7c-b023-a57132bb95ff",
    "visible": true
}