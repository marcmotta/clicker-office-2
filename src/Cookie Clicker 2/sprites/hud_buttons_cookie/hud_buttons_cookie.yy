{
    "id": "a74e7532-5e0b-4593-9632-25ba10d82dd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hud_buttons_cookie",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 21,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd868363-fbee-4415-8386-9fbfe356e853",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a74e7532-5e0b-4593-9632-25ba10d82dd3",
            "compositeImage": {
                "id": "e4859e3b-17c1-4790-bad3-8ec004b55189",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd868363-fbee-4415-8386-9fbfe356e853",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c437fd7-353a-4fc5-b2f0-e47b869f44c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd868363-fbee-4415-8386-9fbfe356e853",
                    "LayerId": "2d29703f-946f-4bd4-bf9e-4fca7f3c7468"
                }
            ]
        },
        {
            "id": "6a2dad97-ad43-4d11-bf58-212f818ebc45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a74e7532-5e0b-4593-9632-25ba10d82dd3",
            "compositeImage": {
                "id": "49b72ed5-5c26-4871-95f6-ad8caa8d33cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a2dad97-ad43-4d11-bf58-212f818ebc45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61b01049-ef13-408f-b0fe-6235b3eeb1e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a2dad97-ad43-4d11-bf58-212f818ebc45",
                    "LayerId": "2d29703f-946f-4bd4-bf9e-4fca7f3c7468"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "2d29703f-946f-4bd4-bf9e-4fca7f3c7468",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a74e7532-5e0b-4593-9632-25ba10d82dd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}