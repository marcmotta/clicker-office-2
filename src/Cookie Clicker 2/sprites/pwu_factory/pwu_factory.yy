{
    "id": "80ab55e6-e549-4bcc-a759-4f094648fbf2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pwu_factory",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d82107c8-cc8d-43f9-a57e-a9c1598e92f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "80ab55e6-e549-4bcc-a759-4f094648fbf2",
            "compositeImage": {
                "id": "f095f7a0-27f7-4a27-b42f-b57729325228",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d82107c8-cc8d-43f9-a57e-a9c1598e92f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "332f97dc-d11f-4f66-b5b9-9400e0451369",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d82107c8-cc8d-43f9-a57e-a9c1598e92f5",
                    "LayerId": "62d78543-b52e-4527-946c-c6b1d47e8226"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "62d78543-b52e-4527-946c-c6b1d47e8226",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "80ab55e6-e549-4bcc-a759-4f094648fbf2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 0,
    "yorig": 0
}