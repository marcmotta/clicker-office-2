{
    "id": "9d88b6de-410f-4e7e-a7d3-a1d34780fd9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_menu_exit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 26,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ae0b4c87-a359-43ff-b817-dd707903d1dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d88b6de-410f-4e7e-a7d3-a1d34780fd9e",
            "compositeImage": {
                "id": "5c3c39ef-37b0-4c1a-a8f9-8b23d68bb882",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae0b4c87-a359-43ff-b817-dd707903d1dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f47bca5-019a-4494-a448-fc05740e704d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae0b4c87-a359-43ff-b817-dd707903d1dd",
                    "LayerId": "098ecdc9-c818-4022-96f7-e18b8779a3ec"
                }
            ]
        },
        {
            "id": "374822c1-2fae-46fa-8646-d682b535a106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d88b6de-410f-4e7e-a7d3-a1d34780fd9e",
            "compositeImage": {
                "id": "6f1d496c-5585-48fa-b35d-3898079a3ea6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "374822c1-2fae-46fa-8646-d682b535a106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75dc5eca-e209-46ed-9a67-5dba9333a379",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "374822c1-2fae-46fa-8646-d682b535a106",
                    "LayerId": "098ecdc9-c818-4022-96f7-e18b8779a3ec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "098ecdc9-c818-4022-96f7-e18b8779a3ec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d88b6de-410f-4e7e-a7d3-a1d34780fd9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 6
}