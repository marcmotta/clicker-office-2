{
    "id": "e4bcd78b-e0ea-4c33-bc3b-73847dcdd847",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_options_hud_slider",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 8,
    "bbox_left": 1,
    "bbox_right": 5,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1c54d83c-5296-4e45-a2b9-fb463dd24fd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4bcd78b-e0ea-4c33-bc3b-73847dcdd847",
            "compositeImage": {
                "id": "df3c3044-165d-4d57-98df-4a8a6e9e5257",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c54d83c-5296-4e45-a2b9-fb463dd24fd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3abde9f-6990-46d2-b220-a4b33518014b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c54d83c-5296-4e45-a2b9-fb463dd24fd6",
                    "LayerId": "b21b0ffa-a865-4cc7-a80a-86622b4a5408"
                }
            ]
        },
        {
            "id": "0185fe41-17c3-4e20-8ded-e8c5df56b45b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4bcd78b-e0ea-4c33-bc3b-73847dcdd847",
            "compositeImage": {
                "id": "a718e5b7-4874-4379-90fe-0296e807060f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0185fe41-17c3-4e20-8ded-e8c5df56b45b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45906102-985c-4db5-aa10-c9bde590dabc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0185fe41-17c3-4e20-8ded-e8c5df56b45b",
                    "LayerId": "b21b0ffa-a865-4cc7-a80a-86622b4a5408"
                }
            ]
        },
        {
            "id": "607a1766-1c97-4ba2-ae40-c137efbf86e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4bcd78b-e0ea-4c33-bc3b-73847dcdd847",
            "compositeImage": {
                "id": "0f4f03af-ef2b-457f-ae19-052fb857f201",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "607a1766-1c97-4ba2-ae40-c137efbf86e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2600a6b-9019-4c4a-983a-33ae32a446f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "607a1766-1c97-4ba2-ae40-c137efbf86e7",
                    "LayerId": "b21b0ffa-a865-4cc7-a80a-86622b4a5408"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 10,
    "layers": [
        {
            "id": "b21b0ffa-a865-4cc7-a80a-86622b4a5408",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4bcd78b-e0ea-4c33-bc3b-73847dcdd847",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 7,
    "xorig": 3,
    "yorig": 5
}