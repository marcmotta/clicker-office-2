{
    "id": "76878f65-43e1-4d41-8b88-701af96f3046",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "chef_strip8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 3,
    "bbox_right": 42,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1e76dfe-4f98-48d2-a18e-4f55c4f88439",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76878f65-43e1-4d41-8b88-701af96f3046",
            "compositeImage": {
                "id": "93bb409b-985b-4bd0-9f9a-8f357d5367bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1e76dfe-4f98-48d2-a18e-4f55c4f88439",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99ceb71a-95e7-4116-8061-ddc7870deeef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1e76dfe-4f98-48d2-a18e-4f55c4f88439",
                    "LayerId": "04895725-f0a9-4c81-bdb7-5a843a0d34f2"
                }
            ]
        },
        {
            "id": "46232bbc-f2e2-40f6-b42c-0f284e21ba03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76878f65-43e1-4d41-8b88-701af96f3046",
            "compositeImage": {
                "id": "34d91348-c800-4c80-8b3b-e018bf97a7a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46232bbc-f2e2-40f6-b42c-0f284e21ba03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70b0041b-7261-4013-a926-f1bd4ce578e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46232bbc-f2e2-40f6-b42c-0f284e21ba03",
                    "LayerId": "04895725-f0a9-4c81-bdb7-5a843a0d34f2"
                }
            ]
        },
        {
            "id": "06cc9591-f5ee-4e4e-8a4a-fd45fdd55593",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76878f65-43e1-4d41-8b88-701af96f3046",
            "compositeImage": {
                "id": "e196ac88-1311-4c73-bdc6-25c5fb58f2d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06cc9591-f5ee-4e4e-8a4a-fd45fdd55593",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0eafa350-1afb-4e8d-89df-e4d7f7591181",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06cc9591-f5ee-4e4e-8a4a-fd45fdd55593",
                    "LayerId": "04895725-f0a9-4c81-bdb7-5a843a0d34f2"
                }
            ]
        },
        {
            "id": "e552a7df-eacf-461f-be24-c5e761e1f52e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76878f65-43e1-4d41-8b88-701af96f3046",
            "compositeImage": {
                "id": "aaeec427-07ca-4f35-899a-b050b3e0b9be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e552a7df-eacf-461f-be24-c5e761e1f52e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78282cb7-5cbb-44f9-9d2f-e7491971f08e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e552a7df-eacf-461f-be24-c5e761e1f52e",
                    "LayerId": "04895725-f0a9-4c81-bdb7-5a843a0d34f2"
                }
            ]
        },
        {
            "id": "23998e31-ade5-41ba-8154-1cdd6914b00d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76878f65-43e1-4d41-8b88-701af96f3046",
            "compositeImage": {
                "id": "e05f9cea-6cc0-4549-9ad8-7544e4664248",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23998e31-ade5-41ba-8154-1cdd6914b00d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fe53eb4-724c-4591-888b-d76b1b255337",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23998e31-ade5-41ba-8154-1cdd6914b00d",
                    "LayerId": "04895725-f0a9-4c81-bdb7-5a843a0d34f2"
                }
            ]
        },
        {
            "id": "cdce8217-54b6-4ce3-86f4-03ff2a7d42c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76878f65-43e1-4d41-8b88-701af96f3046",
            "compositeImage": {
                "id": "153b2c34-f96f-436f-a8ff-8edda17598f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdce8217-54b6-4ce3-86f4-03ff2a7d42c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c575972-1a38-4e24-9c7b-474098128f62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdce8217-54b6-4ce3-86f4-03ff2a7d42c7",
                    "LayerId": "04895725-f0a9-4c81-bdb7-5a843a0d34f2"
                }
            ]
        },
        {
            "id": "198d67f2-9f9a-4c2b-ae4f-39dfe368fc04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76878f65-43e1-4d41-8b88-701af96f3046",
            "compositeImage": {
                "id": "89b78b19-ca98-4047-a607-626723418329",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "198d67f2-9f9a-4c2b-ae4f-39dfe368fc04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "106b75f2-affc-45b6-9e65-3be7de7c8e41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "198d67f2-9f9a-4c2b-ae4f-39dfe368fc04",
                    "LayerId": "04895725-f0a9-4c81-bdb7-5a843a0d34f2"
                }
            ]
        },
        {
            "id": "3e314a37-7a23-4d80-b889-bec2711bcb3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76878f65-43e1-4d41-8b88-701af96f3046",
            "compositeImage": {
                "id": "d8fd7686-0df1-4d8b-bc9e-eb87bc74ed63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e314a37-7a23-4d80-b889-bec2711bcb3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aacfd2f4-ad90-4536-bddc-a42c155cc15a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e314a37-7a23-4d80-b889-bec2711bcb3a",
                    "LayerId": "04895725-f0a9-4c81-bdb7-5a843a0d34f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 65,
    "layers": [
        {
            "id": "04895725-f0a9-4c81-bdb7-5a843a0d34f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76878f65-43e1-4d41-8b88-701af96f3046",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 44,
    "xorig": 22,
    "yorig": 55
}