{
    "id": "5bb71b29-e1cd-4ef7-947a-06f5bebf818c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "credits_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12f1407f-6dc9-429c-b22e-beb1c25cb827",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5bb71b29-e1cd-4ef7-947a-06f5bebf818c",
            "compositeImage": {
                "id": "397e6d6d-cb81-459c-9c96-8bb35de45700",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12f1407f-6dc9-429c-b22e-beb1c25cb827",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d434e9f3-cdea-43c9-81b0-e81585eff90c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12f1407f-6dc9-429c-b22e-beb1c25cb827",
                    "LayerId": "f71bc49c-18a2-466f-a989-991641a320f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "f71bc49c-18a2-466f-a989-991641a320f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5bb71b29-e1cd-4ef7-947a-06f5bebf818c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 268,
    "yorig": 129
}