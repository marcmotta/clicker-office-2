{
    "id": "e7e782bb-1074-488c-82f1-fe406e7f8c98",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "menu_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 52,
    "bbox_left": 1,
    "bbox_right": 95,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c0dd7905-8656-451b-8fa7-540c047fc5a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e7e782bb-1074-488c-82f1-fe406e7f8c98",
            "compositeImage": {
                "id": "a267eb9f-a6b3-40b3-8d8c-cc03f3d9312c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c0dd7905-8656-451b-8fa7-540c047fc5a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "53306a38-71b4-4373-9236-bc7ae67dc183",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c0dd7905-8656-451b-8fa7-540c047fc5a7",
                    "LayerId": "002797cb-9fc1-4cb9-b638-9a2cae7c190f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 54,
    "layers": [
        {
            "id": "002797cb-9fc1-4cb9-b638-9a2cae7c190f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e7e782bb-1074-488c-82f1-fe406e7f8c98",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 97,
    "xorig": 48,
    "yorig": 27
}