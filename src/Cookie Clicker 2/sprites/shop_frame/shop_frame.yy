{
    "id": "5fb18e50-5548-42d4-b935-193becf28192",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "shop_frame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 1,
    "bbox_right": 134,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b9126e40-b207-4d58-bc9c-482c6e5c5c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5fb18e50-5548-42d4-b935-193becf28192",
            "compositeImage": {
                "id": "fa614414-96d7-4caa-b1e5-c80121ea88b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9126e40-b207-4d58-bc9c-482c6e5c5c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0f9e3566-b43e-4ae9-90e4-301382b36b05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9126e40-b207-4d58-bc9c-482c6e5c5c47",
                    "LayerId": "73c9e275-87fe-408c-b51d-c247fb86723d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 98,
    "layers": [
        {
            "id": "73c9e275-87fe-408c-b51d-c247fb86723d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5fb18e50-5548-42d4-b935-193becf28192",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 136,
    "xorig": 68,
    "yorig": 47
}