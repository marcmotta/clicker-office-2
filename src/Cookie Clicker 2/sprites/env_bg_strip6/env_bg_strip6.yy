{
    "id": "fddf5c12-a005-46c7-8653-5c0a7f20618a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "env_bg_strip6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "31cb32c5-28f8-4c1a-922f-beb07f81c1a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddf5c12-a005-46c7-8653-5c0a7f20618a",
            "compositeImage": {
                "id": "da7f8a61-23ce-4f62-bae9-8c48d99799b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31cb32c5-28f8-4c1a-922f-beb07f81c1a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d13f14c7-cd9e-4f3e-add0-d2dc11fcd7e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31cb32c5-28f8-4c1a-922f-beb07f81c1a1",
                    "LayerId": "377158a1-493e-4999-8379-02f13d98a333"
                }
            ]
        },
        {
            "id": "58043947-4517-4ea3-9aa8-0bb5b5136090",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddf5c12-a005-46c7-8653-5c0a7f20618a",
            "compositeImage": {
                "id": "30b5c927-116f-4035-a751-defb44367e40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58043947-4517-4ea3-9aa8-0bb5b5136090",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49c3b3b7-c75a-47bb-8cf3-ad0a3ba24613",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58043947-4517-4ea3-9aa8-0bb5b5136090",
                    "LayerId": "377158a1-493e-4999-8379-02f13d98a333"
                }
            ]
        },
        {
            "id": "2949ab10-c4dc-4b96-9be4-0ee0e9026bb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddf5c12-a005-46c7-8653-5c0a7f20618a",
            "compositeImage": {
                "id": "7c00c225-9643-4cb1-a4d0-3ab0aa8c4104",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2949ab10-c4dc-4b96-9be4-0ee0e9026bb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce0c3491-f65c-4787-ac98-c185e9a45710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2949ab10-c4dc-4b96-9be4-0ee0e9026bb1",
                    "LayerId": "377158a1-493e-4999-8379-02f13d98a333"
                }
            ]
        },
        {
            "id": "751be2f3-e6d7-4996-a8ea-9f804a1e4a54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddf5c12-a005-46c7-8653-5c0a7f20618a",
            "compositeImage": {
                "id": "5f84c2c6-b29e-4ed6-86fb-31bb5b54b299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "751be2f3-e6d7-4996-a8ea-9f804a1e4a54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c6fe597e-76c8-401c-a516-e46177cd4993",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "751be2f3-e6d7-4996-a8ea-9f804a1e4a54",
                    "LayerId": "377158a1-493e-4999-8379-02f13d98a333"
                }
            ]
        },
        {
            "id": "32091cfe-25ca-4148-9642-2a1be672cff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddf5c12-a005-46c7-8653-5c0a7f20618a",
            "compositeImage": {
                "id": "c923c3a7-ea23-4eb6-baed-bfcdbc522196",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32091cfe-25ca-4148-9642-2a1be672cff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23e79862-b278-4592-bd3a-a60868498daa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32091cfe-25ca-4148-9642-2a1be672cff4",
                    "LayerId": "377158a1-493e-4999-8379-02f13d98a333"
                }
            ]
        },
        {
            "id": "375e6f1e-08a9-46fd-98e1-8af40db7da55",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fddf5c12-a005-46c7-8653-5c0a7f20618a",
            "compositeImage": {
                "id": "a80dbbb5-d027-4839-83c7-509bc077efa3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "375e6f1e-08a9-46fd-98e1-8af40db7da55",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f645e1f9-65db-4458-992b-6a38c3862710",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "375e6f1e-08a9-46fd-98e1-8af40db7da55",
                    "LayerId": "377158a1-493e-4999-8379-02f13d98a333"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "377158a1-493e-4999-8379-02f13d98a333",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fddf5c12-a005-46c7-8653-5c0a7f20618a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}