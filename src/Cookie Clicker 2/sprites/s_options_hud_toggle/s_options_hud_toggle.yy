{
    "id": "1e3645be-4b09-4102-a711-0bb1156f9a1c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_options_hud_toggle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19418f81-850d-4828-9b95-c92bdd147278",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e3645be-4b09-4102-a711-0bb1156f9a1c",
            "compositeImage": {
                "id": "578847de-5917-4d10-b5a1-742559d28aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19418f81-850d-4828-9b95-c92bdd147278",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "364a28f3-00f8-47ab-ac99-6945bc93acb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19418f81-850d-4828-9b95-c92bdd147278",
                    "LayerId": "b25a02aa-9a1a-4fd0-bc61-a383f7eef9b1"
                }
            ]
        },
        {
            "id": "c17d4a10-4d89-4b73-b03d-a6ad1ec47f5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e3645be-4b09-4102-a711-0bb1156f9a1c",
            "compositeImage": {
                "id": "c610db4e-13df-41b5-be17-8e8ca77cb011",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c17d4a10-4d89-4b73-b03d-a6ad1ec47f5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5cb66888-6ce0-45af-8fcc-70dcdcef1a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c17d4a10-4d89-4b73-b03d-a6ad1ec47f5e",
                    "LayerId": "b25a02aa-9a1a-4fd0-bc61-a383f7eef9b1"
                }
            ]
        },
        {
            "id": "85028733-a89f-459c-a39b-eaed188b7a40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e3645be-4b09-4102-a711-0bb1156f9a1c",
            "compositeImage": {
                "id": "6c703044-a02a-40cd-847c-5a7e688e111a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85028733-a89f-459c-a39b-eaed188b7a40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "555539fb-c9d3-4014-92dd-3c1cea001f5d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85028733-a89f-459c-a39b-eaed188b7a40",
                    "LayerId": "b25a02aa-9a1a-4fd0-bc61-a383f7eef9b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "b25a02aa-9a1a-4fd0-bc61-a383f7eef9b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e3645be-4b09-4102-a711-0bb1156f9a1c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}