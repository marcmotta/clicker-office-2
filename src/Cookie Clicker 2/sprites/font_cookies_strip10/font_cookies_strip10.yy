{
    "id": "4292a6ac-3690-41d5-8521-4e3584cb2389",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "font_cookies_strip10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 5,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c09d8dfe-ce52-40e1-9ca0-67549b6389bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "compositeImage": {
                "id": "d35637b6-614d-4167-8f76-53c826030a20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c09d8dfe-ce52-40e1-9ca0-67549b6389bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e8ac6dc-368f-47eb-bf19-7bb5e826b877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c09d8dfe-ce52-40e1-9ca0-67549b6389bc",
                    "LayerId": "b07319e6-6279-417c-84f3-930795a95ab0"
                }
            ]
        },
        {
            "id": "62b4daa6-a5fb-4f8a-9bb9-ea041c9053fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "compositeImage": {
                "id": "c3fb2d89-73b8-467b-84cc-cd7fdc146983",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62b4daa6-a5fb-4f8a-9bb9-ea041c9053fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1d7bc72-32d7-4ca5-830a-7f70e2677c2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62b4daa6-a5fb-4f8a-9bb9-ea041c9053fc",
                    "LayerId": "b07319e6-6279-417c-84f3-930795a95ab0"
                }
            ]
        },
        {
            "id": "3ee483b6-f0c1-499f-a3c3-59ea72b8f413",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "compositeImage": {
                "id": "37a57afa-1d55-48d0-a083-3fd9f3032f9d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3ee483b6-f0c1-499f-a3c3-59ea72b8f413",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "49fa96e4-811f-40d1-bc25-f31a4d2f3905",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3ee483b6-f0c1-499f-a3c3-59ea72b8f413",
                    "LayerId": "b07319e6-6279-417c-84f3-930795a95ab0"
                }
            ]
        },
        {
            "id": "1eafc911-d945-4c9f-8b8f-ad6a359380ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "compositeImage": {
                "id": "b13d461d-9699-4727-a76a-8be84f715bb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1eafc911-d945-4c9f-8b8f-ad6a359380ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b481eb7d-efe7-46b9-bc25-5b5675389e18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1eafc911-d945-4c9f-8b8f-ad6a359380ef",
                    "LayerId": "b07319e6-6279-417c-84f3-930795a95ab0"
                }
            ]
        },
        {
            "id": "33230a4e-da53-42a2-9b24-0032b591bbdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "compositeImage": {
                "id": "8bedf6b4-dc19-419d-b85a-62670e7a68f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33230a4e-da53-42a2-9b24-0032b591bbdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6495fdae-ff4a-4f28-9a98-210d62814873",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33230a4e-da53-42a2-9b24-0032b591bbdb",
                    "LayerId": "b07319e6-6279-417c-84f3-930795a95ab0"
                }
            ]
        },
        {
            "id": "8972f350-411c-4f3e-9a7e-ff9bd0f48dc2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "compositeImage": {
                "id": "9aa27c8e-10f5-4e13-8b04-784e6bd838de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8972f350-411c-4f3e-9a7e-ff9bd0f48dc2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a9027a0a-681b-4e38-8994-70e08fcaf040",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8972f350-411c-4f3e-9a7e-ff9bd0f48dc2",
                    "LayerId": "b07319e6-6279-417c-84f3-930795a95ab0"
                }
            ]
        },
        {
            "id": "4ac55e0b-490d-4c91-9fdd-cbf4fc734261",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "compositeImage": {
                "id": "504e6dbb-6d69-439e-ad64-f295f5ac4aa9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ac55e0b-490d-4c91-9fdd-cbf4fc734261",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dabe7f88-3b41-490b-a694-ac0051582209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ac55e0b-490d-4c91-9fdd-cbf4fc734261",
                    "LayerId": "b07319e6-6279-417c-84f3-930795a95ab0"
                }
            ]
        },
        {
            "id": "44eb9d36-9017-4181-92c6-b0f531df7725",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "compositeImage": {
                "id": "c529ac55-c1b6-4dd1-a97b-4f9b8a95974d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44eb9d36-9017-4181-92c6-b0f531df7725",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46bfc97a-e025-4c8b-9b4f-fecb339b80c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44eb9d36-9017-4181-92c6-b0f531df7725",
                    "LayerId": "b07319e6-6279-417c-84f3-930795a95ab0"
                }
            ]
        },
        {
            "id": "f6cce787-88d2-4f18-8129-d7ef1312aa94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "compositeImage": {
                "id": "31106f35-19da-4168-ba89-1f4fa0af490a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6cce787-88d2-4f18-8129-d7ef1312aa94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94d1c759-f7b2-414d-97ef-2c316589a4a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6cce787-88d2-4f18-8129-d7ef1312aa94",
                    "LayerId": "b07319e6-6279-417c-84f3-930795a95ab0"
                }
            ]
        },
        {
            "id": "e1e7e9a4-f26d-4041-9564-94295a8913f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "compositeImage": {
                "id": "1c33cd84-e82e-428a-9332-a8a94aad5928",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1e7e9a4-f26d-4041-9564-94295a8913f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9d2a6795-ecf1-4598-a217-3d8cca2791ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1e7e9a4-f26d-4041-9564-94295a8913f1",
                    "LayerId": "b07319e6-6279-417c-84f3-930795a95ab0"
                }
            ]
        },
        {
            "id": "8da3fc6b-4aaa-46fd-af03-0a15b8e68f47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "compositeImage": {
                "id": "28bb8864-3c8a-4bf8-8899-1a31aa9c72d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8da3fc6b-4aaa-46fd-af03-0a15b8e68f47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6994a0a8-394b-4ce9-91a0-6def2c79aa85",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8da3fc6b-4aaa-46fd-af03-0a15b8e68f47",
                    "LayerId": "b07319e6-6279-417c-84f3-930795a95ab0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 9,
    "layers": [
        {
            "id": "b07319e6-6279-417c-84f3-930795a95ab0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4292a6ac-3690-41d5-8521-4e3584cb2389",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 6,
    "xorig": 0,
    "yorig": 0
}