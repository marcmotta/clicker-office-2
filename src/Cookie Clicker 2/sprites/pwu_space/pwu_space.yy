{
    "id": "ae2a646b-828d-4026-88d0-2eb0e7f775da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pwu_space",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "891c79dc-4c65-4685-8d38-29b73a445e52",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae2a646b-828d-4026-88d0-2eb0e7f775da",
            "compositeImage": {
                "id": "d4218377-a982-46ab-a80e-c3b2860c061d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "891c79dc-4c65-4685-8d38-29b73a445e52",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "367850cb-d630-40e5-9fc6-ad1020cd7d12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "891c79dc-4c65-4685-8d38-29b73a445e52",
                    "LayerId": "4ad8e9a3-1112-44f1-9ea0-24433ab28683"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "4ad8e9a3-1112-44f1-9ea0-24433ab28683",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae2a646b-828d-4026-88d0-2eb0e7f775da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 0,
    "yorig": 0
}