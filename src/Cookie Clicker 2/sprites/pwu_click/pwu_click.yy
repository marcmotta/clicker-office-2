{
    "id": "b6793c69-a29c-40f2-84c0-a066beb4c206",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pwu_click",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f457ace-a83a-4b88-873e-004fd362b969",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6793c69-a29c-40f2-84c0-a066beb4c206",
            "compositeImage": {
                "id": "e83f4cfa-3b8d-42b7-929b-8e8d47014e5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f457ace-a83a-4b88-873e-004fd362b969",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8e30c9e2-4cc3-4dfb-931c-c0fef4d2c1a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f457ace-a83a-4b88-873e-004fd362b969",
                    "LayerId": "8dd8a03f-e1e2-48d8-a1a5-0026102ef82a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "8dd8a03f-e1e2-48d8-a1a5-0026102ef82a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6793c69-a29c-40f2-84c0-a066beb4c206",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 0,
    "yorig": 0
}