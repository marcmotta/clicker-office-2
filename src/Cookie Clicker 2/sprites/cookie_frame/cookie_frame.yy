{
    "id": "5e36c9e2-debd-4e63-ab6b-0330c598e88a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cookie_frame",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 92,
    "bbox_left": 1,
    "bbox_right": 134,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90dbb160-fee7-4f48-8a24-954180ea32a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5e36c9e2-debd-4e63-ab6b-0330c598e88a",
            "compositeImage": {
                "id": "c172ee38-ff49-4319-9494-fcbf2705fb15",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90dbb160-fee7-4f48-8a24-954180ea32a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7b08cd3-f7b3-4296-88a8-3cf1cb1978fd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90dbb160-fee7-4f48-8a24-954180ea32a4",
                    "LayerId": "c7f72d19-d9b4-45c7-a623-6d86ecae5da5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 94,
    "layers": [
        {
            "id": "c7f72d19-d9b4-45c7-a623-6d86ecae5da5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5e36c9e2-debd-4e63-ab6b-0330c598e88a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 136,
    "xorig": 68,
    "yorig": 47
}