{
    "id": "b8d34079-d55c-4693-9592-44ad339da4df",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_options_hud_return",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 1,
    "bbox_right": 58,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 5,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d16faabc-9fc2-4055-ae40-c30a0cb9cf69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8d34079-d55c-4693-9592-44ad339da4df",
            "compositeImage": {
                "id": "9e32b6ca-e9bb-49c3-b427-92180b997d09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d16faabc-9fc2-4055-ae40-c30a0cb9cf69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab278f17-e3e1-46a0-96d2-b3f859231b7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d16faabc-9fc2-4055-ae40-c30a0cb9cf69",
                    "LayerId": "f413ffac-2e5b-41b6-8d6e-badd556ed86a"
                }
            ]
        },
        {
            "id": "9492d8d9-4a13-4402-90b8-26421cec9550",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8d34079-d55c-4693-9592-44ad339da4df",
            "compositeImage": {
                "id": "b7134806-e636-4aef-ba08-5a344f4e2cb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9492d8d9-4a13-4402-90b8-26421cec9550",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d771169f-20c6-4a86-acb2-c74b51d572fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9492d8d9-4a13-4402-90b8-26421cec9550",
                    "LayerId": "f413ffac-2e5b-41b6-8d6e-badd556ed86a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "f413ffac-2e5b-41b6-8d6e-badd556ed86a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8d34079-d55c-4693-9592-44ad339da4df",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 0,
    "yorig": 44
}