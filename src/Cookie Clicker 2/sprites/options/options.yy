{
    "id": "b4d5dedd-1a09-4804-8e99-249e42a06c9a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "options",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2aba2611-6840-4165-91dc-5d06c75f0bcb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4d5dedd-1a09-4804-8e99-249e42a06c9a",
            "compositeImage": {
                "id": "168a6706-bbe2-4cc2-9ae0-eb28d4e3c040",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aba2611-6840-4165-91dc-5d06c75f0bcb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "100acfe5-3423-4f55-a77c-d13ea4e412e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aba2611-6840-4165-91dc-5d06c75f0bcb",
                    "LayerId": "9efb1e52-c421-4cef-8d92-da2aab469313"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "9efb1e52-c421-4cef-8d92-da2aab469313",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4d5dedd-1a09-4804-8e99-249e42a06c9a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}