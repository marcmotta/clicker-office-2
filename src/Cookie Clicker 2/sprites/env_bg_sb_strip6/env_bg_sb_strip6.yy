{
    "id": "ec0047e9-50b0-4e07-b555-be623660e276",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "env_bg_sb_strip6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "299994d8-a44f-4195-ac12-1afc080b8e9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec0047e9-50b0-4e07-b555-be623660e276",
            "compositeImage": {
                "id": "9c7cdc5d-3912-4b08-949a-0aa58f55bc8f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "299994d8-a44f-4195-ac12-1afc080b8e9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bb01f48-41ae-429f-a6e5-e67543373957",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "299994d8-a44f-4195-ac12-1afc080b8e9b",
                    "LayerId": "bbd7e08b-5cd8-47af-8cd2-c2cd88269ec6"
                }
            ]
        },
        {
            "id": "97721a7b-a1f7-40f9-9c33-4bafbad3a64b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec0047e9-50b0-4e07-b555-be623660e276",
            "compositeImage": {
                "id": "ba5ac959-b09a-4da6-8e8c-66df9e105c3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97721a7b-a1f7-40f9-9c33-4bafbad3a64b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf128d08-b456-41c1-a057-3969e57cdb25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97721a7b-a1f7-40f9-9c33-4bafbad3a64b",
                    "LayerId": "bbd7e08b-5cd8-47af-8cd2-c2cd88269ec6"
                }
            ]
        },
        {
            "id": "84bb10ea-50d6-4481-9efd-0a8e3365e300",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec0047e9-50b0-4e07-b555-be623660e276",
            "compositeImage": {
                "id": "b1efd967-bd3c-4536-84f0-68a686f4b5f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84bb10ea-50d6-4481-9efd-0a8e3365e300",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11fb221d-96a9-440e-908c-aa395885539d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84bb10ea-50d6-4481-9efd-0a8e3365e300",
                    "LayerId": "bbd7e08b-5cd8-47af-8cd2-c2cd88269ec6"
                }
            ]
        },
        {
            "id": "a0428f44-447b-4b63-b041-172337c79705",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec0047e9-50b0-4e07-b555-be623660e276",
            "compositeImage": {
                "id": "5f1259c7-05b8-4b9b-8b75-25aa3b26ff20",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0428f44-447b-4b63-b041-172337c79705",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4df84eb6-7f17-4dcb-aa02-b56b5e8b88a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0428f44-447b-4b63-b041-172337c79705",
                    "LayerId": "bbd7e08b-5cd8-47af-8cd2-c2cd88269ec6"
                }
            ]
        },
        {
            "id": "5267d10e-0db8-499c-adc7-38ffb547c8ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec0047e9-50b0-4e07-b555-be623660e276",
            "compositeImage": {
                "id": "35fb4a5b-714a-4994-9041-e85203ea9e96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5267d10e-0db8-499c-adc7-38ffb547c8ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb931cef-9f25-4ac3-8995-5b3f622d4808",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5267d10e-0db8-499c-adc7-38ffb547c8ab",
                    "LayerId": "bbd7e08b-5cd8-47af-8cd2-c2cd88269ec6"
                }
            ]
        },
        {
            "id": "57d77ecd-9616-4b15-8fed-5ee072441b86",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec0047e9-50b0-4e07-b555-be623660e276",
            "compositeImage": {
                "id": "6244aaf6-8e27-40d6-8f96-7ef755510a49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57d77ecd-9616-4b15-8fed-5ee072441b86",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e40a6ab-8651-4821-9599-e202e36978af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57d77ecd-9616-4b15-8fed-5ee072441b86",
                    "LayerId": "bbd7e08b-5cd8-47af-8cd2-c2cd88269ec6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "bbd7e08b-5cd8-47af-8cd2-c2cd88269ec6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec0047e9-50b0-4e07-b555-be623660e276",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}