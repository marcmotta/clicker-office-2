{
    "id": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "font_cookiep_sec",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d552992c-110a-458d-8598-683ab9de285c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "e008dbfd-4f1c-4c50-a6b6-fb79a2f5cd5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d552992c-110a-458d-8598-683ab9de285c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d47b5af-357d-48c4-acaf-7888be5e4dcb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d552992c-110a-458d-8598-683ab9de285c",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "e81adcf6-a725-4fd0-be8e-71cf98eb29df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "8c648dae-a94c-4d36-93c8-7f7d270b79ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e81adcf6-a725-4fd0-be8e-71cf98eb29df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc59434-4263-4a6f-aaf3-2a09cb34e62d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e81adcf6-a725-4fd0-be8e-71cf98eb29df",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "bf1fcd17-bb21-4654-81e8-13d94667093b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "f4ce6d2d-d34f-40dc-ab08-2dcd11d0fda7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf1fcd17-bb21-4654-81e8-13d94667093b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1ab1ff80-c61c-4413-b267-7719dc149404",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf1fcd17-bb21-4654-81e8-13d94667093b",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "a7fa7e2c-adc5-4094-83db-1d7a68107fb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "b03664b2-eb7c-4d6e-a096-0412226eacee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7fa7e2c-adc5-4094-83db-1d7a68107fb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4950ef7-e25e-4e32-844a-f40dda88d4f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7fa7e2c-adc5-4094-83db-1d7a68107fb0",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "550368aa-fb1d-427f-9647-23188c906ff3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "06fd8c83-f448-4d2a-9e82-fb62c0b358a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "550368aa-fb1d-427f-9647-23188c906ff3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0e669b4-36e5-4d1c-ab2b-d4e5b630eba7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "550368aa-fb1d-427f-9647-23188c906ff3",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "dfbde167-a821-46bc-81a8-913ad2c1e94e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "46718b15-6db8-442c-b835-6f73bdd6f334",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfbde167-a821-46bc-81a8-913ad2c1e94e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57490550-1926-4686-b225-f9f9d8b55916",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfbde167-a821-46bc-81a8-913ad2c1e94e",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "6a307a9f-64fb-476f-9b4e-9a269a97a4d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "2d330944-981a-418c-92d7-ec9ac82e5b41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a307a9f-64fb-476f-9b4e-9a269a97a4d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30a03dd6-9db0-49fc-8bc9-4b583aaaa70d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a307a9f-64fb-476f-9b4e-9a269a97a4d4",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "bcf5269a-2c0f-4708-9da8-f01ecf7dc945",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "96b65cd6-59d7-4e9a-9d31-fa533ef1aa02",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcf5269a-2c0f-4708-9da8-f01ecf7dc945",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "774e6c9d-e3e9-47ac-97c3-a55b068cb28a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcf5269a-2c0f-4708-9da8-f01ecf7dc945",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "839ad629-24d4-4183-9774-0267e7bc22c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "ab4f5494-c6bd-4a4d-89a7-671a28b6d551",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "839ad629-24d4-4183-9774-0267e7bc22c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5140ef02-8dfa-4353-a598-b0088b7470e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "839ad629-24d4-4183-9774-0267e7bc22c4",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "ee8b88bf-30b6-419b-b92c-c79999e43285",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "c7861a9d-5f71-4950-8f10-ef5eee8ff49b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee8b88bf-30b6-419b-b92c-c79999e43285",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "adc0cc63-a4ca-42cc-9a39-eaebf29f0807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee8b88bf-30b6-419b-b92c-c79999e43285",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "533e4ac0-7d79-4ed1-8b80-fa21224ddee7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "a70ba4bc-1e7c-402b-b7f0-9ee5118cbee0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "533e4ac0-7d79-4ed1-8b80-fa21224ddee7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c654930-e71a-465a-89c3-af08f1c2099d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "533e4ac0-7d79-4ed1-8b80-fa21224ddee7",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "e2eee641-f9a1-4de2-b723-aecdee435e36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "31de09c4-73d4-4b0b-88a8-9a4c9936a92a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2eee641-f9a1-4de2-b723-aecdee435e36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c94551f5-b670-42ed-8276-0323eefa7f6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2eee641-f9a1-4de2-b723-aecdee435e36",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "f1e47422-9e4f-4dee-83b8-5dde23636444",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "f4a91274-c316-45c9-a283-34de65780458",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1e47422-9e4f-4dee-83b8-5dde23636444",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "318b19c7-5e65-4180-a22a-579b38320fd9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1e47422-9e4f-4dee-83b8-5dde23636444",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "1b053fe1-a95a-4603-9be6-fba6de6fb123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "8ff39f22-5d76-4654-acea-d7f17a611084",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b053fe1-a95a-4603-9be6-fba6de6fb123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "992478e7-0920-4ff1-9729-dd429fe2d8e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b053fe1-a95a-4603-9be6-fba6de6fb123",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "8ea51b4a-cf9c-4b3d-8da7-835fce9c227b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "e71306a6-7c21-4183-9d64-0dd28c64f8a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8ea51b4a-cf9c-4b3d-8da7-835fce9c227b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a80df8ec-e094-4cef-8a75-a9c24e9b8223",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8ea51b4a-cf9c-4b3d-8da7-835fce9c227b",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "83644a40-d5ce-4715-a864-7099d2347805",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "2123ddbd-c06d-4662-826e-de9ecc5367a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83644a40-d5ce-4715-a864-7099d2347805",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a4d65e9-ce0d-4dfb-b4b5-a9a68b358010",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83644a40-d5ce-4715-a864-7099d2347805",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        },
        {
            "id": "5f6d65c7-342b-41d8-ad94-7e04e1452bb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "compositeImage": {
                "id": "2fdc7a82-96d9-48d0-b2b9-5330f0adb90f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f6d65c7-342b-41d8-ad94-7e04e1452bb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e79003af-7fb2-40ca-8809-00e4ec1f849b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f6d65c7-342b-41d8-ad94-7e04e1452bb7",
                    "LayerId": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "699c8225-f7ee-4cfb-ad5e-7ac6e1ae9902",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9afd46d1-c70e-4e58-b8b3-04f8c996fc88",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}