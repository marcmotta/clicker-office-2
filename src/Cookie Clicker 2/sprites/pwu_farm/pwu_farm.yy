{
    "id": "48210256-c1bb-475a-bfbb-54387301ed0b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pwu_farm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a9c38d54-4709-49a7-a581-6e5e9ba724b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48210256-c1bb-475a-bfbb-54387301ed0b",
            "compositeImage": {
                "id": "4bc0e560-8430-429a-97bc-128550ccba5a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a9c38d54-4709-49a7-a581-6e5e9ba724b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f72f2ec-0df1-4063-a89b-471e09211efe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a9c38d54-4709-49a7-a581-6e5e9ba724b9",
                    "LayerId": "0184bd10-4058-4766-af9d-9dc05db39083"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "0184bd10-4058-4766-af9d-9dc05db39083",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48210256-c1bb-475a-bfbb-54387301ed0b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}