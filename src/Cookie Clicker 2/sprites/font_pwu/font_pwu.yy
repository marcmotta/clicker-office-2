{
    "id": "7adada3f-e514-477b-938c-0ca090e6c120",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "font_pwu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 3,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a4f59048-6c61-433f-b660-c5c93608c2ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "compositeImage": {
                "id": "b2fcaf16-def8-4088-ae92-fd2192df19c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a4f59048-6c61-433f-b660-c5c93608c2ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64c13790-7167-4e90-a835-2b5f54a151e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a4f59048-6c61-433f-b660-c5c93608c2ba",
                    "LayerId": "d45c763e-080b-4614-883d-1b5bacc4d11e"
                }
            ]
        },
        {
            "id": "553b239d-6137-40bf-ba64-d12b28010d4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "compositeImage": {
                "id": "84d18205-fc86-446d-a4ed-7592345ac399",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "553b239d-6137-40bf-ba64-d12b28010d4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a251ec8-aedb-4d7d-9bac-7cb888c3ee68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "553b239d-6137-40bf-ba64-d12b28010d4d",
                    "LayerId": "d45c763e-080b-4614-883d-1b5bacc4d11e"
                }
            ]
        },
        {
            "id": "db82ca7f-8e34-462f-a38f-48214edc90b4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "compositeImage": {
                "id": "4a0c99a0-dfec-4c97-b96c-04a5bb6f4ee2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db82ca7f-8e34-462f-a38f-48214edc90b4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4b0d165-7210-4440-ab98-ac82490edd25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db82ca7f-8e34-462f-a38f-48214edc90b4",
                    "LayerId": "d45c763e-080b-4614-883d-1b5bacc4d11e"
                }
            ]
        },
        {
            "id": "4acbb509-be3d-46e1-bd03-1c37527e4517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "compositeImage": {
                "id": "bea899c6-3aec-4808-80c6-e8256cc8f5b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4acbb509-be3d-46e1-bd03-1c37527e4517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "913115ab-6861-4b42-b1d4-6714bba2c66f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4acbb509-be3d-46e1-bd03-1c37527e4517",
                    "LayerId": "d45c763e-080b-4614-883d-1b5bacc4d11e"
                }
            ]
        },
        {
            "id": "5b570b30-2b9e-4215-bc66-692cdd2b4a80",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "compositeImage": {
                "id": "42410e49-a4ae-4d98-8c50-9bb09a77c026",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b570b30-2b9e-4215-bc66-692cdd2b4a80",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca6567a2-41ad-42f8-9869-f06144c1c759",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b570b30-2b9e-4215-bc66-692cdd2b4a80",
                    "LayerId": "d45c763e-080b-4614-883d-1b5bacc4d11e"
                }
            ]
        },
        {
            "id": "e1358d46-33c4-4e19-a3f7-b59fa5416a33",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "compositeImage": {
                "id": "e72c03a8-a841-4c7a-908a-a4263a068810",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1358d46-33c4-4e19-a3f7-b59fa5416a33",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02a59246-daca-4fce-ad08-dd12c0b1e668",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1358d46-33c4-4e19-a3f7-b59fa5416a33",
                    "LayerId": "d45c763e-080b-4614-883d-1b5bacc4d11e"
                }
            ]
        },
        {
            "id": "ca849a77-3761-436f-9262-6aec09557a1f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "compositeImage": {
                "id": "aea3a0ba-1eea-4981-847e-2f63b8c5ebdb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca849a77-3761-436f-9262-6aec09557a1f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4f778f31-0d03-4cd3-a89c-a7b906157cd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca849a77-3761-436f-9262-6aec09557a1f",
                    "LayerId": "d45c763e-080b-4614-883d-1b5bacc4d11e"
                }
            ]
        },
        {
            "id": "8eb20ee4-0eec-452f-9658-62452e17f53a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "compositeImage": {
                "id": "f27d3530-f656-4ca9-a3b8-ec96e450eb18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8eb20ee4-0eec-452f-9658-62452e17f53a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aadd5cf4-719e-44b3-bdc3-5dca4723c30b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8eb20ee4-0eec-452f-9658-62452e17f53a",
                    "LayerId": "d45c763e-080b-4614-883d-1b5bacc4d11e"
                }
            ]
        },
        {
            "id": "3b3b36ba-f58e-4b98-83bb-1a9eee893c43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "compositeImage": {
                "id": "da5457fe-017b-4a5d-bfbf-25231a9a770f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b3b36ba-f58e-4b98-83bb-1a9eee893c43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed6fb922-11a8-453d-bceb-e3a57e7cae2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b3b36ba-f58e-4b98-83bb-1a9eee893c43",
                    "LayerId": "d45c763e-080b-4614-883d-1b5bacc4d11e"
                }
            ]
        },
        {
            "id": "417b55f4-ed33-4702-a42e-fb9a1e618c27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "compositeImage": {
                "id": "1c439f45-6286-43f5-887c-3d2536b11996",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "417b55f4-ed33-4702-a42e-fb9a1e618c27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb90a861-933c-47b2-a580-f9531e15b1f0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "417b55f4-ed33-4702-a42e-fb9a1e618c27",
                    "LayerId": "d45c763e-080b-4614-883d-1b5bacc4d11e"
                }
            ]
        },
        {
            "id": "dd3330a6-21cc-42b4-8a60-6f2708d82753",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "compositeImage": {
                "id": "9d668a16-42db-43ef-9997-08e0492c55b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd3330a6-21cc-42b4-8a60-6f2708d82753",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb26db0f-8cff-444f-a025-cf361a970b59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd3330a6-21cc-42b4-8a60-6f2708d82753",
                    "LayerId": "d45c763e-080b-4614-883d-1b5bacc4d11e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 7,
    "layers": [
        {
            "id": "d45c763e-080b-4614-883d-1b5bacc4d11e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7adada3f-e514-477b-938c-0ca090e6c120",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}