{
    "id": "186baf84-2d3c-4ce6-8ce7-5b246652cad0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pwu_granma",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fe28888a-564d-42dc-8cfd-bf34b75ff0b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "186baf84-2d3c-4ce6-8ce7-5b246652cad0",
            "compositeImage": {
                "id": "a90258c3-6fb0-4a24-a578-50834ed35ccc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe28888a-564d-42dc-8cfd-bf34b75ff0b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15b2b795-80de-4bb9-b4c9-458f42589564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe28888a-564d-42dc-8cfd-bf34b75ff0b5",
                    "LayerId": "b76e3cb9-2d6b-4caa-83ba-a1157c420498"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "b76e3cb9-2d6b-4caa-83ba-a1157c420498",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "186baf84-2d3c-4ce6-8ce7-5b246652cad0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}