{
    "id": "169374f9-9add-4589-9937-40457e7c4e15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pwu_mine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "56d7fb92-c3ae-4be2-b80e-8c8bb92f51b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "169374f9-9add-4589-9937-40457e7c4e15",
            "compositeImage": {
                "id": "a684ca8d-96a2-4cd7-88cd-2e5a6e1c22d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "56d7fb92-c3ae-4be2-b80e-8c8bb92f51b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72b106cf-ff43-47bc-9d96-53f267932599",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "56d7fb92-c3ae-4be2-b80e-8c8bb92f51b5",
                    "LayerId": "dc89f750-b33a-4f86-ad23-72a07eab9add"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "dc89f750-b33a-4f86-ad23-72a07eab9add",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "169374f9-9add-4589-9937-40457e7c4e15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 0,
    "yorig": 0
}