{
    "id": "b4edfbcb-ef77-4b7c-b023-a57132bb95ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hud_danger_strip2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2a1f7c2-aa09-42a9-84c4-9128a8a05e08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4edfbcb-ef77-4b7c-b023-a57132bb95ff",
            "compositeImage": {
                "id": "f99a2562-5849-4b39-b196-2761da48f0ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2a1f7c2-aa09-42a9-84c4-9128a8a05e08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c34e4622-8c6d-42e4-8ed3-334ea7c7f4d9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2a1f7c2-aa09-42a9-84c4-9128a8a05e08",
                    "LayerId": "bf89178b-90c0-4821-a82a-53131d84fc44"
                }
            ]
        },
        {
            "id": "1a2f86cc-0c63-4e04-9c43-93163efc7ced",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4edfbcb-ef77-4b7c-b023-a57132bb95ff",
            "compositeImage": {
                "id": "3ebe1493-3ca3-47d2-98a9-0debf4fba9c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a2f86cc-0c63-4e04-9c43-93163efc7ced",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1e2092e-5227-4a57-9648-e4b7c5893067",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a2f86cc-0c63-4e04-9c43-93163efc7ced",
                    "LayerId": "bf89178b-90c0-4821-a82a-53131d84fc44"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 27,
    "layers": [
        {
            "id": "bf89178b-90c0-4821-a82a-53131d84fc44",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4edfbcb-ef77-4b7c-b023-a57132bb95ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 1,
    "yorig": 1
}