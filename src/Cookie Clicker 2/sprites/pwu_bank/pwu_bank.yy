{
    "id": "3ebc7b07-41fb-4248-9423-b62b83f648fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pwu_bank",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22e8975e-a759-4360-b0ef-7a7664efbcb6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ebc7b07-41fb-4248-9423-b62b83f648fa",
            "compositeImage": {
                "id": "0be8ba35-5e4b-457e-849d-370722400bf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22e8975e-a759-4360-b0ef-7a7664efbcb6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af8cdfee-a85a-4374-afa9-0c005d69d463",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22e8975e-a759-4360-b0ef-7a7664efbcb6",
                    "LayerId": "5a21f736-4f4c-418c-bf25-fe2a5e4c40b5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "5a21f736-4f4c-418c-bf25-fe2a5e4c40b5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ebc7b07-41fb-4248-9423-b62b83f648fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}