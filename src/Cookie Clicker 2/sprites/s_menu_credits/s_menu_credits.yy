{
    "id": "10e5e816-40d6-464a-934a-39685ca42fff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_menu_credits",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 1,
    "bbox_right": 55,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c448ba0-0145-40c4-883a-cd8b1b33a19e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e5e816-40d6-464a-934a-39685ca42fff",
            "compositeImage": {
                "id": "afc07cd9-62e8-4156-9a1b-935c282eae3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c448ba0-0145-40c4-883a-cd8b1b33a19e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5bb01d4-9d72-43ec-990c-4aaef0ded290",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c448ba0-0145-40c4-883a-cd8b1b33a19e",
                    "LayerId": "08f74e57-b0bc-47a2-992a-22713e4d71bb"
                }
            ]
        },
        {
            "id": "f1d7b281-b3e1-428d-8d09-6184af389652",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "10e5e816-40d6-464a-934a-39685ca42fff",
            "compositeImage": {
                "id": "d9ebfb53-d0ba-4a96-86bf-74a819b324fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1d7b281-b3e1-428d-8d09-6184af389652",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "709049e8-eac1-4cd5-9a39-c64be1877973",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1d7b281-b3e1-428d-8d09-6184af389652",
                    "LayerId": "08f74e57-b0bc-47a2-992a-22713e4d71bb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 15,
    "layers": [
        {
            "id": "08f74e57-b0bc-47a2-992a-22713e4d71bb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "10e5e816-40d6-464a-934a-39685ca42fff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 57,
    "xorig": 28,
    "yorig": 7
}