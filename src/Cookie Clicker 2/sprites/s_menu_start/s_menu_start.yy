{
    "id": "9c6c33b2-0992-47a5-835a-65a621288f18",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_menu_start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 37,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2b22ab99-8d91-40a3-9ce8-2c64d9d16615",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c6c33b2-0992-47a5-835a-65a621288f18",
            "compositeImage": {
                "id": "cfd81502-a9c0-4cdb-9335-03aef138288f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2b22ab99-8d91-40a3-9ce8-2c64d9d16615",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5ff3f7d-2fd3-4985-bdba-568a1e7ce607",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2b22ab99-8d91-40a3-9ce8-2c64d9d16615",
                    "LayerId": "a04c4f10-a018-4deb-9b0a-8637257f8ed9"
                }
            ]
        },
        {
            "id": "c3fe356a-7ff0-4d2a-ade0-caa7bcb3f228",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9c6c33b2-0992-47a5-835a-65a621288f18",
            "compositeImage": {
                "id": "83836f7f-4cb1-4e56-ac75-1386594af7cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3fe356a-7ff0-4d2a-ade0-caa7bcb3f228",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82bb49e0-f4ac-418f-b111-2ee47cf6c3b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3fe356a-7ff0-4d2a-ade0-caa7bcb3f228",
                    "LayerId": "a04c4f10-a018-4deb-9b0a-8637257f8ed9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "a04c4f10-a018-4deb-9b0a-8637257f8ed9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9c6c33b2-0992-47a5-835a-65a621288f18",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 39,
    "xorig": 19,
    "yorig": 8
}