{
    "id": "753527c4-7768-4a4e-8df5-99f0c53156c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "pwu_museum",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 39,
    "bbox_left": 0,
    "bbox_right": 30,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cbc0fd59-01ab-4a55-a0c9-d8e61d203cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "753527c4-7768-4a4e-8df5-99f0c53156c8",
            "compositeImage": {
                "id": "7ebe5c96-67d6-4e81-b0de-6db2b7c5e157",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbc0fd59-01ab-4a55-a0c9-d8e61d203cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d242d32-e770-45ba-9439-acaa0913e8f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbc0fd59-01ab-4a55-a0c9-d8e61d203cde",
                    "LayerId": "bf956374-5d3f-43bd-8ac9-7a8d6bbfc561"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 40,
    "layers": [
        {
            "id": "bf956374-5d3f-43bd-8ac9-7a8d6bbfc561",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "753527c4-7768-4a4e-8df5-99f0c53156c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 31,
    "xorig": 0,
    "yorig": 0
}