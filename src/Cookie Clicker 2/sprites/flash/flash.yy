{
    "id": "2abf9935-c06f-4fcb-9fd7-e30f3842c916",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "flash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "122e3a97-1b1a-4000-9059-0570b43bab22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2abf9935-c06f-4fcb-9fd7-e30f3842c916",
            "compositeImage": {
                "id": "4c36fb42-3790-42cb-9d71-8a44e575e55a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "122e3a97-1b1a-4000-9059-0570b43bab22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15e9abfb-73f0-4278-88c6-4696824877ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "122e3a97-1b1a-4000-9059-0570b43bab22",
                    "LayerId": "6c49b741-3ebf-4aa3-ada4-cb31bdf2cea8"
                }
            ]
        },
        {
            "id": "19adc158-ef01-4a31-86a4-143a6981e8c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2abf9935-c06f-4fcb-9fd7-e30f3842c916",
            "compositeImage": {
                "id": "e9ca3eff-98e7-4a64-a82d-bf4cfa4d10d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19adc158-ef01-4a31-86a4-143a6981e8c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd388ebd-cb02-4bc7-a7ad-9100bc54fe86",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19adc158-ef01-4a31-86a4-143a6981e8c7",
                    "LayerId": "6c49b741-3ebf-4aa3-ada4-cb31bdf2cea8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "6c49b741-3ebf-4aa3-ada4-cb31bdf2cea8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2abf9935-c06f-4fcb-9fd7-e30f3842c916",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}