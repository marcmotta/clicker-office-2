{
    "id": "7518e66d-43d1-465f-9d41-63892536b7e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hud_lifes_strip4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 1,
    "bbox_right": 47,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f641bf7e-c041-4c45-aec5-882f6646b4e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7518e66d-43d1-465f-9d41-63892536b7e0",
            "compositeImage": {
                "id": "477945d3-81a9-4734-931d-7dc35fc0f767",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f641bf7e-c041-4c45-aec5-882f6646b4e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa060427-c7b1-4eba-a1b7-b705968ac523",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f641bf7e-c041-4c45-aec5-882f6646b4e8",
                    "LayerId": "777e87a3-e3fd-4644-a817-66de84709ac8"
                }
            ]
        },
        {
            "id": "c1127e83-7ed9-4e7e-ade5-0a7915e480bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7518e66d-43d1-465f-9d41-63892536b7e0",
            "compositeImage": {
                "id": "1d66de0c-90aa-41c5-9e09-ec77d2cbc9f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1127e83-7ed9-4e7e-ade5-0a7915e480bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd5be81b-2f8d-495c-8e2c-f5860badd503",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1127e83-7ed9-4e7e-ade5-0a7915e480bf",
                    "LayerId": "777e87a3-e3fd-4644-a817-66de84709ac8"
                }
            ]
        },
        {
            "id": "543a6474-1451-405c-a0bd-8a4f64c94803",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7518e66d-43d1-465f-9d41-63892536b7e0",
            "compositeImage": {
                "id": "aa3b3356-22e7-488b-83cc-efb54d1aa2c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "543a6474-1451-405c-a0bd-8a4f64c94803",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "747352cd-60b8-4d6a-8285-c99e8a01dcf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "543a6474-1451-405c-a0bd-8a4f64c94803",
                    "LayerId": "777e87a3-e3fd-4644-a817-66de84709ac8"
                }
            ]
        },
        {
            "id": "42266b76-6b47-4b79-8ef8-30c155fccaf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7518e66d-43d1-465f-9d41-63892536b7e0",
            "compositeImage": {
                "id": "c5902584-0336-4b6a-81f2-601fc829521b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42266b76-6b47-4b79-8ef8-30c155fccaf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1b2a531-4433-43f1-98e9-2f8cd31d32db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42266b76-6b47-4b79-8ef8-30c155fccaf7",
                    "LayerId": "777e87a3-e3fd-4644-a817-66de84709ac8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 14,
    "layers": [
        {
            "id": "777e87a3-e3fd-4644-a817-66de84709ac8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7518e66d-43d1-465f-9d41-63892536b7e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 2,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 48,
    "yorig": 0
}