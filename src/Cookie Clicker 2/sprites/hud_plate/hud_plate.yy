{
    "id": "29b7c7a7-5c77-4d4a-af2a-a952b5363e9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hud_plate",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 1,
    "bbox_right": 124,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d51d66be-8efc-4d99-9a17-f889a1c367b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29b7c7a7-5c77-4d4a-af2a-a952b5363e9e",
            "compositeImage": {
                "id": "ac61d675-bab5-441c-ba90-cd05a9e9c1b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d51d66be-8efc-4d99-9a17-f889a1c367b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02cb4a8d-8337-45a2-8409-c2ef3bf535b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d51d66be-8efc-4d99-9a17-f889a1c367b0",
                    "LayerId": "150a3b9e-b7e5-4bd2-b55f-6d543be46561"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "150a3b9e-b7e5-4bd2-b55f-6d543be46561",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29b7c7a7-5c77-4d4a-af2a-a952b5363e9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 63,
    "yorig": 19
}