{
    "id": "3e1e512e-e3e8-4ac2-a8cc-01c509cac5f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hud_gameover",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 96,
    "bbox_left": 101,
    "bbox_right": 217,
    "bbox_top": 69,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34870190-6116-4f5b-b4ad-18b48d92c1bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e1e512e-e3e8-4ac2-a8cc-01c509cac5f9",
            "compositeImage": {
                "id": "411620ba-ad58-4089-b945-2676ce095357",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34870190-6116-4f5b-b4ad-18b48d92c1bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1824ed3-676c-4f52-83ed-718e604f9661",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34870190-6116-4f5b-b4ad-18b48d92c1bf",
                    "LayerId": "eeffb030-17e6-48f3-8f5f-8f3d9f746a25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "eeffb030-17e6-48f3-8f5f-8f3d9f746a25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e1e512e-e3e8-4ac2-a8cc-01c509cac5f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 90
}