{
    "id": "5848dfa8-cebb-4ec9-877a-9fb79b20b79e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "credits_text_strip2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 303,
    "bbox_left": 105,
    "bbox_right": 217,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adc17e08-8d8c-4baa-a322-a532cec16bfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5848dfa8-cebb-4ec9-877a-9fb79b20b79e",
            "compositeImage": {
                "id": "6f8dad8d-594d-439e-8207-89a9d13fd3eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adc17e08-8d8c-4baa-a322-a532cec16bfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5913b2df-09fa-4e39-b890-ca4c2f1e2bfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adc17e08-8d8c-4baa-a322-a532cec16bfa",
                    "LayerId": "61a600c2-f1cf-4db1-8321-e3b90e3c06c0"
                }
            ]
        },
        {
            "id": "ff441c3b-b36f-495c-a9eb-a1f29adb93a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5848dfa8-cebb-4ec9-877a-9fb79b20b79e",
            "compositeImage": {
                "id": "e8f2178b-98a2-476f-82e7-b8a92ec80ad6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ff441c3b-b36f-495c-a9eb-a1f29adb93a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3728794f-c697-4d44-876e-f97d148c66d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ff441c3b-b36f-495c-a9eb-a1f29adb93a6",
                    "LayerId": "61a600c2-f1cf-4db1-8321-e3b90e3c06c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 312,
    "layers": [
        {
            "id": "61a600c2-f1cf-4db1-8321-e3b90e3c06c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5848dfa8-cebb-4ec9-877a-9fb79b20b79e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 1,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 160,
    "yorig": 0
}