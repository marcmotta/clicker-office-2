{
    "id": "31035cb5-d085-4f5e-8a19-17420d79997d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 179,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c159a89-c8c7-48a4-945f-aa2b42a1ca79",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "31035cb5-d085-4f5e-8a19-17420d79997d",
            "compositeImage": {
                "id": "69993994-98af-470a-8be5-ce49f7c1c682",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c159a89-c8c7-48a4-945f-aa2b42a1ca79",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfc57e47-f0dd-4a33-b4b2-a48c605e4960",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c159a89-c8c7-48a4-945f-aa2b42a1ca79",
                    "LayerId": "8f5025cf-2696-4c38-9b74-066e0ef4453d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 180,
    "layers": [
        {
            "id": "8f5025cf-2696-4c38-9b74-066e0ef4453d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "31035cb5-d085-4f5e-8a19-17420d79997d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}