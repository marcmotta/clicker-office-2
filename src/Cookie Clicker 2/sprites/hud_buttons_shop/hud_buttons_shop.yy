{
    "id": "dafdd77f-2090-4e40-b329-5bd0a7691b71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hud_buttons_shop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 1,
    "bbox_right": 21,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3de6a3f-e9c8-45f5-a078-cd7c64995e2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dafdd77f-2090-4e40-b329-5bd0a7691b71",
            "compositeImage": {
                "id": "2d60ecb2-8a6d-4f3e-8391-f3cb9cf95224",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3de6a3f-e9c8-45f5-a078-cd7c64995e2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9093675-15a4-445b-ab8a-b31b37d06d99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3de6a3f-e9c8-45f5-a078-cd7c64995e2c",
                    "LayerId": "43d6792e-9f74-4d08-b5fd-2866a3510513"
                }
            ]
        },
        {
            "id": "36d8b483-5736-4991-b213-f04fd52e7c3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dafdd77f-2090-4e40-b329-5bd0a7691b71",
            "compositeImage": {
                "id": "2f7d580f-da88-46e6-b652-aec04fd27d7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36d8b483-5736-4991-b213-f04fd52e7c3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22c3784a-10c7-4060-83ad-845d3915d9fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36d8b483-5736-4991-b213-f04fd52e7c3d",
                    "LayerId": "43d6792e-9f74-4d08-b5fd-2866a3510513"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "43d6792e-9f74-4d08-b5fd-2866a3510513",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dafdd77f-2090-4e40-b329-5bd0a7691b71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 23,
    "xorig": 0,
    "yorig": 0
}