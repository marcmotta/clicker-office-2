{
    "id": "5006e2f5-296b-4957-bcfa-4521324980b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hud_cursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 8,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6185dc0f-a21b-4580-88ac-e61d9bb6c330",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5006e2f5-296b-4957-bcfa-4521324980b8",
            "compositeImage": {
                "id": "996121d6-409a-4f5a-acec-6481bcb1f0fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6185dc0f-a21b-4580-88ac-e61d9bb6c330",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c72f53-7727-406c-b85c-bdb62520af2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6185dc0f-a21b-4580-88ac-e61d9bb6c330",
                    "LayerId": "5dc09093-8a25-4381-8a8f-c0a13ee3b827"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "5dc09093-8a25-4381-8a8f-c0a13ee3b827",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5006e2f5-296b-4957-bcfa-4521324980b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 10,
    "xorig": 1,
    "yorig": 1
}