{
    "id": "8e51c460-6947-49e1-a44c-3071b36aaf68",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "hud_buttons_new1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 1,
    "bbox_right": 16,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "04c71085-cec9-4177-89c9-f886f044f70d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8e51c460-6947-49e1-a44c-3071b36aaf68",
            "compositeImage": {
                "id": "361ac9e6-d6b9-4d1f-a89f-fbc6d1e712fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "04c71085-cec9-4177-89c9-f886f044f70d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "02584001-ce54-4ae1-8378-da21fae7d959",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "04c71085-cec9-4177-89c9-f886f044f70d",
                    "LayerId": "acb4613a-4b3b-40ba-b4c4-2fff05460099"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 11,
    "layers": [
        {
            "id": "acb4613a-4b3b-40ba-b4c4-2fff05460099",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8e51c460-6947-49e1-a44c-3071b36aaf68",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 18,
    "xorig": 9,
    "yorig": 9
}