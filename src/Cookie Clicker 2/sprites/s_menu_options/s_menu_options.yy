{
    "id": "008652c8-d7a3-481e-a561-fa12028d2b02",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_menu_options",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 1,
    "bbox_right": 47,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd4ccd22-e425-4768-9be1-58fa91a535ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008652c8-d7a3-481e-a561-fa12028d2b02",
            "compositeImage": {
                "id": "7d3862fb-2a3c-464a-aaef-e86988a427d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd4ccd22-e425-4768-9be1-58fa91a535ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d78802d-f646-49c3-b317-ee0d6859490a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd4ccd22-e425-4768-9be1-58fa91a535ac",
                    "LayerId": "03e9b98b-a09c-4000-a64f-5e30630f7d87"
                }
            ]
        },
        {
            "id": "2e8cb77e-af12-48dc-a08d-90861e3067a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "008652c8-d7a3-481e-a561-fa12028d2b02",
            "compositeImage": {
                "id": "0b8c3d61-1071-4457-a3f8-53e7be147ead",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e8cb77e-af12-48dc-a08d-90861e3067a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "952e6eef-0bea-4e04-bc14-6ccd05c1fa05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e8cb77e-af12-48dc-a08d-90861e3067a7",
                    "LayerId": "03e9b98b-a09c-4000-a64f-5e30630f7d87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 18,
    "layers": [
        {
            "id": "03e9b98b-a09c-4000-a64f-5e30630f7d87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "008652c8-d7a3-481e-a561-fa12028d2b02",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 24,
    "yorig": 9
}