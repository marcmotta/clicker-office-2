{
    "id": "405555a0-bcfa-41b7-bb17-59b89a2a33d7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "cookie_strip2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 1,
    "bbox_right": 26,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72f1577f-2a2f-4394-9e14-a6a6cb11782f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "405555a0-bcfa-41b7-bb17-59b89a2a33d7",
            "compositeImage": {
                "id": "7099ddb2-d7c6-4cbd-9293-c5b8b81c2e45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72f1577f-2a2f-4394-9e14-a6a6cb11782f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b91037d-ceb0-4212-bae2-ad149d15e860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72f1577f-2a2f-4394-9e14-a6a6cb11782f",
                    "LayerId": "df3f44ff-1317-4061-a4ad-4e7f30826bb0"
                }
            ]
        },
        {
            "id": "22a20e77-8af9-4aa1-99b2-af01f90cbd5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "405555a0-bcfa-41b7-bb17-59b89a2a33d7",
            "compositeImage": {
                "id": "16350bf9-353c-4f07-9e7c-6dea0fe5fbea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22a20e77-8af9-4aa1-99b2-af01f90cbd5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a337977a-6e71-423b-9841-397ce545a2bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22a20e77-8af9-4aa1-99b2-af01f90cbd5f",
                    "LayerId": "df3f44ff-1317-4061-a4ad-4e7f30826bb0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 29,
    "layers": [
        {
            "id": "df3f44ff-1317-4061-a4ad-4e7f30826bb0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "405555a0-bcfa-41b7-bb17-59b89a2a33d7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 14
}