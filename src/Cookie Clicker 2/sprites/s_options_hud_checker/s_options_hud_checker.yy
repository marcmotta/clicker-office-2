{
    "id": "f7d5b395-2826-43dc-9281-27242a071edb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_options_hud_checker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 2,
    "bbox_right": 14,
    "bbox_top": 3,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "010e4339-9fa2-4c00-8794-bc36c0cd2cf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7d5b395-2826-43dc-9281-27242a071edb",
            "compositeImage": {
                "id": "a2e02952-b01c-4e66-838c-04772284d15b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "010e4339-9fa2-4c00-8794-bc36c0cd2cf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1044a18d-8f74-48a5-b55d-63e629709f25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "010e4339-9fa2-4c00-8794-bc36c0cd2cf2",
                    "LayerId": "23956288-fbfe-4696-b806-7ecfc288903e"
                }
            ]
        },
        {
            "id": "b367f039-ba63-421f-a0ad-dfa774288078",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7d5b395-2826-43dc-9281-27242a071edb",
            "compositeImage": {
                "id": "09bf34e2-e7b0-4af9-85fd-ec7bbb42d80b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b367f039-ba63-421f-a0ad-dfa774288078",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79becae5-4088-4e65-b661-c065d5c47e01",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b367f039-ba63-421f-a0ad-dfa774288078",
                    "LayerId": "23956288-fbfe-4696-b806-7ecfc288903e"
                }
            ]
        },
        {
            "id": "d9031e24-ed7c-4fc3-a18b-54e5acdd9b8c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f7d5b395-2826-43dc-9281-27242a071edb",
            "compositeImage": {
                "id": "d26d841f-fee2-4ce1-b388-27591c862e94",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9031e24-ed7c-4fc3-a18b-54e5acdd9b8c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "23b120fc-92ac-461d-90e5-3b5bd5cffecb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9031e24-ed7c-4fc3-a18b-54e5acdd9b8c",
                    "LayerId": "23956288-fbfe-4696-b806-7ecfc288903e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "23956288-fbfe-4696-b806-7ecfc288903e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f7d5b395-2826-43dc-9281-27242a071edb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 8,
    "yorig": 9
}